import {Nav} from 'components/common';
import Head from 'next/head';

export default function Projects() {
  return (
    <div>
      <Head>
        <title>Projects | Project Manager</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <Nav />
    </div>
  );
}
