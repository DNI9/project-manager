import {FormHeader, SignupForm} from 'components/Forms';
import Link from 'next/link';

export default function Signup() {
  return (
    <div className='container flex flex-col max-w-lg min-h-screen mx-auto space-y-5 pt-14'>
      <FormHeader
        title="Let's get started"
        subTitle='Manage projects and tasks with easy and clean interface.'
      />
      <SignupForm />
      <p className='mx-auto mt-5 text-gray-200'>
        already have account?{' '}
        <span className='text-green-light'>
          <Link href='/auth'>login here</Link>
        </span>
      </p>
    </div>
  );
}
