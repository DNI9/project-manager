import {FormHeader, LoginForm} from 'components/Forms';
import Link from 'next/link';

export default function Login() {
  return (
    <div className='container flex flex-col max-w-lg min-h-screen mx-auto space-y-5 pt-14'>
      <FormHeader
        title='Welcome back.'
        subTitle='Login to continue to dashboard'
      />
      <LoginForm />
      <p className='mx-auto mt-5 text-gray-200'>
        new here?{' '}
        <span className='text-green-light'>
          <Link href='/auth/signup'>signup now</Link>
        </span>
      </p>
    </div>
  );
}
