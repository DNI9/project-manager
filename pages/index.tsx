import Head from 'next/head';

export default function Home() {
  return (
    <div>
      <Head>
        <title>Home | Project Manager</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <div className='block w-full text-5xl bg-teal'>DASHBOARD</div>
    </div>
  );
}
