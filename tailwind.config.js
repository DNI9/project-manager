module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        blue: {
          lightest: '#457899',
          light: '#85d7ff',
          DEFAULT: '#1fb6ff',
          dark: '#22577A',
          darkest: '#1F3747',
        },
        green: {
          light: '#80ED99',
          DEFAULT: '#57CC99',
        },
        teal: {
          DEFAULT: '#38A3A5',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/line-clamp')],
};
