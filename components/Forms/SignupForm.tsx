import {yupResolver} from '@hookform/resolvers/yup';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {FormButton} from '.';
import {Input} from './Input';

export type SignupFormData = {
  name: string;
  email: string;
  password: string;
};

const SignupSchema = Yup.object({
  name: Yup.string().required().min(3).max(50),
  email: Yup.string().email('please type a valid email').required(),
  password: Yup.string().min(6).max(25).required(),
}).required();

export const SignupForm = () => {
  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<SignupFormData>({
    resolver: yupResolver(SignupSchema),
  });

  const onSubmit = handleSubmit(data => console.log(data));
  return (
    <form onSubmit={onSubmit} className='flex flex-col'>
      <Input label='name' register={register} placeholder='your full name' />
      <p className='text-red-400'>{errors.name?.message}</p>

      <Input
        label='email'
        register={register}
        type='email'
        placeholder='john@email.com'
      />
      <p className='text-red-400'>{errors.email?.message}</p>

      <Input
        label='password'
        type='password'
        register={register}
        placeholder='a strong password'
      />
      <p className='text-red-400'>{errors.password?.message}</p>

      <FormButton label='signup' />
    </form>
  );
};
