import {yupResolver} from '@hookform/resolvers/yup';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {FormButton} from '.';
import {Input} from './Input';

export type LoginFormData = {
  email: string;
  password: string;
};

const SignupSchema = Yup.object({
  email: Yup.string().email('please type a valid email').required(),
  password: Yup.string().min(6).max(25).required(),
}).required();

export const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: {errors},
  } = useForm<LoginFormData>({
    resolver: yupResolver(SignupSchema),
  });

  const onSubmit = handleSubmit(data => console.log(data));
  return (
    <form onSubmit={onSubmit} className='flex flex-col'>
      <Input
        label='email'
        register={register}
        type='email'
        placeholder='john@email.com'
      />
      <p className='text-red-400'>{errors.email?.message}</p>

      <Input
        label='password'
        type='password'
        register={register}
        placeholder='a strong password'
      />
      <p className='text-red-400'>{errors.password?.message}</p>

      <FormButton label='login' />
    </form>
  );
};
