import React from 'react';

export const FormButton = ({label}: {label: string}) => {
  return (
    <button
      type='submit'
      className='h-12 mt-3 text-lg font-medium tracking-widest uppercase rounded-md text-blue-darkest bg-gradient-to-r to-green from-teal'>
      {label}
    </button>
  );
};
