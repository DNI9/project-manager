type Props = {
  title?: string;
  subTitle: string;
};

export const FormHeader = ({
  title = 'login/signup to continue',
  subTitle,
}: Props) => {
  return (
    <header>
      <h1 className='text-6xl leading-tight text-transparent bg-clip-text bg-gradient-to-r from-green-light via-green-light to-teal'>
        {title}
      </h1>
      <p className='text-lg opacity-75'>{subTitle}</p>
    </header>
  );
};
