import {HTMLInputTypeAttribute} from 'react';
import {Path, UseFormRegister} from 'react-hook-form';

type Data = Record<string, string>;

type InputProps<T extends Data> = {
  label: Path<T>;
  register: UseFormRegister<T>;
  type?: HTMLInputTypeAttribute;
  placeholder?: string;
};

export const Input = <T extends Data>({
  label,
  register,
  type = 'text',
  placeholder,
}: InputProps<T>) => (
  <>
    <label className='mt-3 ml-1 text-sm font-medium tracking-wider capitalize'>
      {label}
    </label>
    <input
      className='h-12 px-2 placeholder-gray-900 rounded-md outline-none placeholder-opacity-80 focus:border-2 focus:border-green bg-blue-dark'
      {...register(label)}
      type={type}
      placeholder={placeholder}
    />
  </>
);
