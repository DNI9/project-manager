import {MenuIcon, SearchIcon} from '@heroicons/react/solid';
import Image from 'next/image';
import {getAvatar} from 'utils';

export const Nav = () => {
  return (
    <header className='flex items-center justify-between px-5 shadow-xl h-14'>
      <MenuIcon className='cursor-pointer h-7' />
      <div
        id='search-bar'
        className='relative w-1/2 rounded-md bg-blue-dark bg-opacity-30'>
        <SearchIcon className='absolute w-5 h-5 -translate-y-1/2 top-1/2 left-2 text-blue-lightest' />
        <input
          type='search'
          name='search'
          id='search'
          placeholder='search tasks, projects, etc.'
          className='w-full px-8 bg-transparent rounded-md h-11 placeholder-blue-lightest'
        />
      </div>
      <div
        className='relative w-10 h-10 rounded-full cursor-pointer'
        id='avatar'>
        <Image
          src={getAvatar('dni9')}
          alt='avatar of dni9'
          layout='fill'
          objectFit='cover'
        />
      </div>
    </header>
  );
};
