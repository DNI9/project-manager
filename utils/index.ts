const AVATAR_API_URL = 'https://avatars.dicebear.com/api';

export function getAvatar(name: string): string {
  return `${AVATAR_API_URL}/bottts/${name}.svg?r=50`;
}
